const functions = require("firebase-functions");

exports.updateHighscores = functions
    .region("europe-west1")
    .database.ref("/Streams/{userID}/{runID}")
    .onCreate((snapshot, context) => {
      // const run = snapshot.val();
      functions.logger.log("Processing Run by User ", context.params.userID);
      const allData = snapshot.toJSON();
      allData["userID"] = context.params.userID;
      const ghosts = allData["ghosts"];
      delete allData["ghosts"];
      const data = allData;


      return snapshot.ref.root.child("CloudFunctions")
          .child(context.params.userID)
          .child("score").get().then((score) => {
            data["ghostRuns"] = 1;
            if (score.val()) {
              // There is already a score in db
              if (score.val() < data["score"]) {
                // New score is higher
                snapshot.ref.root.child("CloudFunctions")
                    .child(context.params.userID)
                    .set(data).then(function() {
                      functions.logger.log("Higher score updated");
                    });
              } else {
                // Old score is higher or evan
                functions.logger.log("Old score was higher");
              }
            } else {
              // This is no score in db
              snapshot.ref.root.child("CloudFunctions")
                  .child(context.params.userID)
                  .set(data).then(function() {
                    functions.logger.log("New Score inserted");
                  });
            }
            functions.logger.log("Aggregate ghostRuns");
            for (const key in ghosts) {
              snapshot.ref.root.child("CloudFunctions")
                .child(ghosts[key])
                  .child("ghostRuns")
                  .get().then(function(ghostRuns) {
                    if (ghostRuns.val()) {
                      snapshot.ref.root.child("CloudFunctions")
                        .child(ghosts[key])
                          .child("ghostRuns")
                          .set(ghostRuns.val() + 1)
                              .then(function() {
                                functions.logger.log("ghostsRun");
                              });
                    }
                  });
              console.log(ghosts[key]);
            }
          });
    });
