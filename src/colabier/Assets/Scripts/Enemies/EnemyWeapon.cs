using UnityEngine;

public class EnemyWeapon : MonoBehaviour
{
    private int _iteration;
    // Update is called once per frame
    void Update()
    {
        if (GameController.Instance?.Paused == true) {
            return;
        }
        var angle = Utilities.Mod(_iteration*6, 360);
        var eulerAngles = transform.eulerAngles;
        eulerAngles.z = angle;
        transform.eulerAngles = eulerAngles;

        
    }
    void FixedUpdate() {
        if (GameController.Instance?.Paused == true) {
            return;
        }
        _iteration++;
    }

    void OnTriggerEnter2D(Collider2D other) {
        other.gameObject.GetComponent<PlayerHealth>()?.ApplyDamage();        
    }

}
