using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;


public class BasicEnemy : MonoBehaviour, IDamageable
{
    // enemy specific 
    public GameObject target;

    private MovementController _movementController;

    [Header("Health and Damage")]
    public float MaxHP;

    [HideInInspector]
    public float HealthFactor = 1f;
    private float CurrentHP;
    public string DamagedSound;
    public string DeathSound;

    public ParticleSystem DamagedPS;
    public GameObject DiedPS;
    public bool useNavMesh = false;

    private Seeker _seeker;
    private Path _path;
    private int _currentWaypoint = 0;
    private bool _reachedEndOfPath;
    private float _nextWaypointDistance = 0.1f;
    private float _lastPathUpdate = 0;

    void Awake()
    {
    }
    // Start is called before the first frame update
    void Start()
    {
        CurrentHP = MaxHP * HealthFactor;
        if (target == null) {
            target = ChooseRandomTarget();
        }
        _movementController = GetComponent<MovementController>();
        if (useNavMesh)
        {
            _seeker = GetComponent<Seeker>();
            UpdatePath(target.transform.position);
        }
    }

    void Update() {
        if (GameController.Instance?.Paused == true) {
            return;
        }
        // ReselectTarget if it died.
        if (target == null || target.activeSelf == false) {
            target = ChooseRandomTarget();
            if (useNavMesh)
            {
                UpdatePath(target.transform.position);
            }
        }
        if (useNavMesh && Time.time - _lastPathUpdate > 1)
        {
            UpdatePath(target.transform.position);
        }

    }


    void FixedUpdate() {
        // Don't do stuff if Game Paused
        if (GameController.Instance?.Paused == true)
        {
            return;
        }
        if (!useNavMesh)
        {
            _movementController.Movement = Vector3.Normalize(target.transform.position - transform.position);
            _movementController.Rotation = Quaternion.Euler(0, 0, 45 / 2) * Vector3.Normalize(target.transform.position - transform.position);
        } else
        {
            MoveAlongPath();
        }
    }

    private void MoveAlongPath()
    {
        if (_path == null)
        {
            return;
        }

        // Check in a loop if we are close enough to the current waypoint to switch to the next one.
        // We do this in a loop because many waypoints might be close to each other and we may reach
        // several of them in the same frame.
        _reachedEndOfPath = false;
        // The distance to the next waypoint in the path
        float distanceToWaypoint;
        while (true)
        {
            // If you want maximum performance you can check the squared distance instead to get rid of a
            // square root calculation. But that is outside the scope of this tutorial.
            distanceToWaypoint = Vector3.Distance(transform.position, _path.vectorPath[_currentWaypoint]);
            if (distanceToWaypoint < _nextWaypointDistance)
            {
                // Check if there is another waypoint or if we have reached the end of the path
                if (_currentWaypoint + 1 < _path.vectorPath.Count)
                {
                    _currentWaypoint++;
                }
                else
                {
                    // Set a status variable to indicate that the agent has reached the end of the path.
                    // You can use this to trigger some special code if your game requires that.
                    _reachedEndOfPath = true;
                    break;
                }
            }
            else
            {
                break;
            }
        }
        if (_reachedEndOfPath)
        {
            UpdatePath(target.transform.position);
            _movementController.Movement = Vector3.zero;
        } else
        {
            Vector3 myPoint = transform.position + new Vector3(0, -0.5f, 0) * 0.0f;
            Vector3 nextPoint = _path.vectorPath[_currentWaypoint] + new Vector3(0, 0.5f, 0) * 0.0f;
            //if (_path.vectorPath.Count > _currentWaypoint+1)
                //nextPoint = _path.vectorPath[_currentWaypoint+1] + new Vector3(0, 0.5f, 0) * 0.0f;
            Vector3 targetDirection = Vector3.Normalize(nextPoint - myPoint);
            _movementController.Movement = targetDirection;
            _movementController.Rotation = Quaternion.Euler(0, 0, 45 / 2) * targetDirection;
        }
    }

    private GameObject ChooseRandomTarget() {
        // Currently go for player or ghost. Feel Free to improve logic
        GameObject target;
        var ghosts = GameObject.FindGameObjectsWithTag("Ghost");
        var randomTargetIndex = Random.Range(0,2);
        if (ghosts.Length == 0 || randomTargetIndex > 0) {
            target = GameObject.FindGameObjectWithTag("Player");
        }
        else {
            var randomGhostIndex = Random.Range(0, ghosts.Length);
            target = ghosts[randomGhostIndex];
        }
        return target;
    }

    private void UpdatePath(Vector3 target)
    {
        _seeker.StartPath(transform.position, target, OnPathCalculationComplete);
        _lastPathUpdate = Time.time;
    }

    private void OnPathCalculationComplete(Path p)
    {
        if (!p.error)
        {
            _path = p;
            _currentWaypoint = 0;
        }
    }
   
    public void ApplyDamage(float damage = 10f)
    {
        CurrentHP -= damage;

        if (CurrentHP <= 0)
        {
            CurrentHP = 0;
            Die();
        }
        else
        {
            AudioController.Instance.PlaySound(DamagedSound);
            DamagedPS.Play();
        }
    }

    public void Die()
    {
        GameController.Instance.currentEnemies -= 1;
        Instantiate(DiedPS, transform.position, transform.rotation);
        AudioController.Instance.PlaySound(DeathSound);
        // we might need to wait before destroying the object immediately (playing animation or smth)
        Destroy(this.gameObject);
    }
}




