using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicMonsterspawner : MonoBehaviour
{
    public BasicEnemy Spawnable;
    
    public float SpeedIncrease = 0.1f;

    public float CooldownReduce = 0.1f;

    public float MinCooldown = 1f;

    private int _iterations;

    public float Cooldown = 5f;
    public float MovementSpeed = 2f;

    // Start is called before the first frame update
    void Start()
    {
        TrySpawn();
    }

    // Needed so pausing doesnt mess spawning up.
    void TrySpawn() {
        Invoke("TrySpawn", Cooldown);
        Spawn();
    }

    void Spawn() {
        if (GameController.Instance?.Paused == true || GameController.Instance.currentEnemies > GameController.Instance.maxEnimies) {
            return;
        }
        var enemy = Instantiate<BasicEnemy>(Spawnable, transform.position, Quaternion.identity);
        enemy.HealthFactor = Mathf.Max(1, _iterations / 10);
        enemy.GetComponent<MovementController>().MovementSpeed = MovementSpeed;
        Destroy(enemy.gameObject, 30f);
        MovementSpeed = Mathf.Min(15, MovementSpeed + SpeedIncrease);
        Cooldown = Mathf.Max(MinCooldown, Cooldown - CooldownReduce);
        _iterations++;
        GameController.Instance.currentEnemies++;

    }
}
