public interface IDamageable
{
    /// <summary>
    /// Applying damage to the Damageable thing. this SHOULD checks if the thing dies after it got the damage 
    /// </summary>
    /// <param name="damage"></param>
    public void ApplyDamage(float damage);
    public void Die();
}
