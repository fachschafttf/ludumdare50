using UnityEngine;
using UnityEngine.UI;

// ReSharper disable once CheckNamespace
public class SettingsSlider : MonoBehaviour
{
    
    public Slider MusicSlider;
    public Slider SfxSlider;

    // ReSharper disable once UnusedMember.Local
    void Start()
    {
        AdjustSlidersToCurrentLevel();
    }

    public void AdjustSlidersToCurrentLevel()
    {
        MusicSlider.value = AudioController.Instance.MusicVolume;
        SfxSlider.value = AudioController.Instance.SfxVolume;
    }

    // ReSharper disable once UnusedMember.Global
    public void SetMusicVolume(float val)
    {
        AudioController.Instance.SetMusicVolume((int)val);
    }

    // ReSharper disable once UnusedMember.Global
    public void SetSfxVolume(float val)
    {
        AudioController.Instance.SetSfxVolume((int)val);
    }

}
