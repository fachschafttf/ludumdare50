using UnityEngine;

public class HpBarBehavior : MonoBehaviour
{
    [Header("Height of Health bar")]
    public float HEIGHT = .5f;
    [Header("Width of Health bar")]
    public float WIDTH = 4;
  
    [Header("Parts of health bar")]
    public GameObject greenBar;
    public GameObject redBar;
    
    private SpriteRenderer redSpriteRenderer;
    private SpriteRenderer greenSpriteRenderer;

    [Header("Follow the player?")]
    public bool ShouldFollowThePlayer = true;
    public GameObject ObjectToFollow;
    public Vector3 PositionOffset;

    private PlayerHealth _followerPlayerHealth;


    void Start()
    {
        redSpriteRenderer = redBar.GetComponent<SpriteRenderer>();
        greenSpriteRenderer = greenBar.GetComponent<SpriteRenderer>();
        greenSpriteRenderer.transform.localScale = new Vector3(10,HEIGHT,0);
        if (ObjectToFollow == null)
        {
            ObjectToFollow = gameObject.transform.parent.gameObject;
        }
        _followerPlayerHealth = ObjectToFollow.GetComponent<PlayerHealth>();
    
        
    }
    void Update()
    {
        var lifeBarWidth = WIDTH * _followerPlayerHealth.CurrentHP / 100f;
        greenSpriteRenderer.transform.localPosition = new Vector3(lifeBarWidth / 2f, 0, 0);
        greenSpriteRenderer.transform.localScale = new Vector3(lifeBarWidth, HEIGHT, 0);
        
        redSpriteRenderer.transform.localPosition = new Vector3(lifeBarWidth+(WIDTH-lifeBarWidth)/2f, 0, 0);
        redSpriteRenderer.transform.localScale = new Vector3(WIDTH-lifeBarWidth, HEIGHT, 0);
    }

    private void LateUpdate()
    {
        if (ShouldFollowThePlayer)
        {
            var newPos = ObjectToFollow.transform.position + PositionOffset;
            transform.position = newPos;
        }
    }
}
