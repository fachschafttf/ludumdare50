using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Submission : MonoBehaviour
{
    public TMP_InputField PlayerName;
    
    void OnEnable() {
        var name = GameController.Instance.PlayerName;
        
        PlayerName.text = string.IsNullOrWhiteSpace(name) || string.IsNullOrEmpty(name) ? "Anon" : name;
    }
    
    public void UpdatePlayername(string name) {
        GameController.Instance.PlayerName = name;
        PlayerPrefs.SetString("PlayerName", name);
    }

    public void Submit() {
        var score = GameController.Instance.Score;
        var playerName = GameController.Instance.PlayerName;
        Debug.Log($"Submitting {score} for {playerName}");
        PlayerRecorder.Instance?.SubmitStreamToDatabase(playerName, score);
    }
}
