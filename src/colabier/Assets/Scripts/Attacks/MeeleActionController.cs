using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeeleActionController : MonoBehaviour
{
    private List<GameObject> TriggeredEnemies = new List<GameObject>();
    [Tooltip("The string that is used in the tag of the enemies")]
    public string targetType = "enemy";

    /// <summary>
    /// Simple Meele Attack
    /// </summary>
    public void Slash(float damageValue=10){
        var count = TriggeredEnemies.Count;

        // we iterate backwards - it might be possible that we remove objects while iterating!
        for(int i = count - 1; i >= 0; i--)
        {
            if (i >= TriggeredEnemies.Count) {
                continue;
            }
            var damageAble = TriggeredEnemies[i].GetComponent<IDamageable>();
            damageAble.ApplyDamage(damageValue);
        }
    }

    void OnTriggerEnter2D(Collider2D col){
        // Add all enemy objects in rage into the list
        if (col.gameObject.CompareTag(targetType))
        {
        // if (col.gameObject.tag == targetType && ! TriigertEnemies.Contains(col.gameObject)){ //May be unnessesary load
            // Debug.Log(col.gameObject);
            TriggeredEnemies.Add(col.gameObject);
        }
    }
    void OnTriggerExit2D(Collider2D col)
    {
        // Removes Enemy objects out of the list
        if (TriggeredEnemies.Contains(col.gameObject)){
            // Debug.Log(col.gameObject);
            TriggeredEnemies.Remove(col.gameObject);
        }
    }
}
