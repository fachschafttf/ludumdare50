using UnityEngine;
//This can be used for Projectlies or as Monsterspawner
public class ProjectileFactory : MonoBehaviour
{
    [Tooltip("Type of Projectile that will be Crated")]
    public GameObject projectileType; // will contain the PreFab for the Current Projectile

    [Tooltip("Part of the Prefap that defines the start position of our Projectile")]
    public GameObject startPossition;
    private PlayerControls GunHolderPlayer;
    private GhostController GunHolderGhost;

    private int _soundCooldown;
    // Start is called before the first frame update
    void Start()
    {
        GunHolderPlayer = gameObject.GetComponentInParent<PlayerControls>();
        GunHolderGhost = gameObject.GetComponentInParent<GhostController>();
    }

    void FixedUpdate() {
        if (_soundCooldown > 0) {
            _soundCooldown--;
        }
    }

    public GameObject Shoot()
        // This function will spwan a new Pojectile
    {
        if (GameController.Instance.Paused == true) {
            return null;
        }
        if (_soundCooldown > 0) {
            return null;
        }
        _soundCooldown = 5;
        var startposition = startPossition.transform.position;
        var startdirection = startPossition.transform.rotation;
        var created = Instantiate(projectileType, startposition,  startdirection);
        PlayShootingSound();
        return created;
    }

    public void PlayShootingSound() {
        var soundName = "laserShoot";
        if (GunHolderPlayer != null) {
            AudioController.Instance?.PlaySound(soundName);
        }
        else if (GunHolderGhost != null) {
            AudioController.Instance?.PlaySoundRelativeToDistance(soundName, GunHolderGhost.DistanceToPlayer());
        }
    }
}
