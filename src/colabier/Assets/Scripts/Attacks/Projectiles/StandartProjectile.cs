using UnityEngine;

namespace Attacks.Projectiles
{
    public class StandartProjectile : MonoBehaviour
    {
        // Position and Stuff
        public float MovementSpeed = 0.5f;
        private Vector3 _movement ;
        public GameObject Controller;


        // enemy specific 
        // Start is called before the first frame update
        void Start()
        {
            _movement = transform.right;
            Destroy(gameObject, 1f);
        }

        // Update is called once per frame
        void Update()
        {
            Move();
        }

        
        private void Move()
        {

            float step = MovementSpeed * Time.deltaTime;

        
            // If need is there to move in faced direction instead of absolut
            // Rotate movement direction input vector, to match view direction of camera
            Vector3 movementDirection = Quaternion.Euler(0, transform.eulerAngles.y, 0) * _movement;
            transform.position = Vector3.MoveTowards(transform.position, transform.position + movementDirection, step);
        }

    }
}
