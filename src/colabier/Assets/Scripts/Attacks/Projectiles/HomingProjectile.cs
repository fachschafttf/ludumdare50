using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomingProjectile : MonoBehaviour
{
     // Position and Stuff
        public float MovementSpeed = 0.5f;
        private Vector3 _movement;

        public Directions PlayerDirection = Directions.North;

        public string Target;

        public GameObject target;
        // enemy specific 
        // Start is called before the first frame update
        void Start()
        {
            target = GameObject.FindGameObjectWithTag(Target);
        }

        // Update is called once per frame
        void Update()
        {
            UpdateDirections();
            Move();
            Rotate();
        }
         private void UpdateDirections(){
                _movement =   Vector3.Normalize(target.transform.position - transform.position);
            }
        
        
            // Moves the camera relatively towards the view vector (W moves camera in direction forward of view perspective
            private void Move()
            {
                if (_movement.Equals(new Vector3(0,0,0)))
                {
                    return;
                }
        
                float step = MovementSpeed * Time.deltaTime;
        
                
                // If need is there to move in faced direction instead of absolut
                // Rotate movement direction input vector, to match view direction of camera
                bool moveInFacedDirection = false;
                Vector3 movementDirection = moveInFacedDirection ? Quaternion.Euler(0, transform.eulerAngles.y, 0) * _movement : _movement;
        
        
                transform.position = Vector3.MoveTowards(transform.position, transform.position + movementDirection, step);
            }
        
        
            private void Rotate()
            {
                // Naive 2D rotation with 4 Directions
                if( _movement.Equals(new Vector3(0,0,0)))
                {
                    return;
                }
        
                PlayerDirection = Utilities.TransformVectorInDirection(_movement);
        
                var rotation = transform.eulerAngles;
        
        
                switch (PlayerDirection)
                {
                    case Directions.North:
                        rotation.z = 90;
                        break;
                    case Directions.East:
                        rotation.z = 0;
                        break;
                    case Directions.South:
                        rotation.z = 270;
                        break;
                    case Directions.West:
                        rotation.z = 180;
                        break;
                }
        
                transform.eulerAngles = rotation;
            }

        
    
}
