using Attacks.Projectiles;
using UnityEngine;

public class BullitAOEConnection : MonoBehaviour
{
    private MeeleActionController AOE;


    // Start is called before the first frame update
    void Awake()
    {
        AOE = GetComponent<MeeleActionController>();
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        AOE.Slash(4);
        gameObject.GetComponent<StandartProjectile>().Controller.GetComponent<HighscoreTracker>().hit();
        Destroy(this.gameObject);
    }
}
