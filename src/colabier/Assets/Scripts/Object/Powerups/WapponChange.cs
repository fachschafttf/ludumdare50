using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WapponChange : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            var act = col.gameObject.GetComponent<ActionController>();
            act.Range = ! act.Range;
            Destroy(this.gameObject);
        }
    }
}
