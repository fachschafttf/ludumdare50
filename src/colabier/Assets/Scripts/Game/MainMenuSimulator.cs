using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuSimulator : MonoBehaviour
{
    public MainMenuPlayerController MenuPlayerPrefab;
    public BasicEnemy EnemyPrefab;
    public int SpawnCooldown = 2;
    public int StartDelay = 1;
    public Vector3 MovementDirection;
    public Directions FaceDirection;

    private MainMenuPlayerController _spawnedPlayer;


    void Start()
    {
        Invoke("Spawn", StartDelay);

    }

    private void Spawn()
    {
        _spawnedPlayer = Instantiate(MenuPlayerPrefab, transform.position, Quaternion.identity);
        _spawnedPlayer.Movement = MovementDirection;
        _spawnedPlayer.Rotation = MovementDirection;
        _spawnedPlayer.FaceDirection = FaceDirection;
        Destroy(_spawnedPlayer.gameObject, 30f);
        for (var i = 0; i < 4; i++) {
            var enemy =  Instantiate(EnemyPrefab, transform.position - MovementDirection * (i+4), Quaternion.identity);
            enemy.target = _spawnedPlayer.gameObject;
            Destroy(enemy.gameObject, 30f);
        }
        
        
        Invoke("Spawn", SpawnCooldown);
    }


}
