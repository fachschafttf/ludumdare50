using System.Collections.Generic;
using System;
using UnityEngine;
using Utility;

[Serializable]
public class PlayerRecorder : MonoBehaviour
{
    public static PlayerRecorder Instance;

    private InputStream _inputStream;

    void Awake() {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        _inputStream = new InputStream();
    }

    public void RegisterPlayerUpdate(Vector3 movement, Vector3 rotation, bool actionExecuted)
    {
        var userInput = new UserInput();
        var factor = 1000;
        userInput.M = new Vector2Int((int)(movement.x * factor), (int)(movement.y * factor));
        userInput.R = new Vector2Int((int)(rotation.x * factor), (int)(rotation.y * factor));
        userInput.B = actionExecuted;
        _inputStream.Stream.Add( userInput);
    }

    public void SubmitStreamToDatabase(String selectedUsername, int score) {
        //PlayerPlayback.Instance?.AddInputStream(_inputStream);
        Debug.Log("Writing Stream to Firebase");
        FirebaseClient client = new FirebaseClient();
        _inputStream.username = selectedUsername;
        _inputStream.score = score;
        StartCoroutine(client.Post("Streams").Uid().Execute(SerializeToJson()));
    }

    private string SerializeToJson()
    {
        var compressedStream = new InputStreamCompressed();
        compressedStream.CompressInputStream(_inputStream);

        InputStreamUpload up = InputStreamUpload.fromInputStream(compressedStream);
        up.ghosts = PlayerPlayback.Instance!.GetLoadedGhostNames();
        return JsonUtility.ToJson(up);
    }
}

[Serializable]
public class UserInput {
    public Vector2Int M;
    public Vector2Int R;
    public bool B;
}

[Serializable]
public class InputStreamCompressed {
    public List<UserInputCounter> CompressedStream = new List<UserInputCounter>();
    public String username;
    public int score;
    public String userID;


    public void CompressInputStream(InputStream stream) {
        username = stream.username;
        score = stream.score;
        userID = stream.userID;

        var inputStream = stream.Stream;
        
        UserInput currentInput = null;
        // First Iteration sets to 1 anyway.
        var repeatings = 0;

        //Debug.Log($"{inputStream.Count} initial Elements");
        for(var i = 0; i < inputStream.Count; i++) {
            //First iteration
            if (currentInput == null) {
                currentInput = inputStream[i];
            }

            if (IsIdentical(inputStream[i], currentInput)) {   
                repeatings++;
            }
            else {
                var userInputCounter = new UserInputCounter();
                userInputCounter.U = currentInput;
                userInputCounter.C = repeatings;
                CompressedStream.Add(userInputCounter);
                repeatings = 1;
                currentInput = inputStream[i];
            }
        }
        var userInputCounter2 = new UserInputCounter();
        userInputCounter2.U = currentInput;
        userInputCounter2.C = repeatings;
        // Last Iteration
        CompressedStream.Add(userInputCounter2); 

        //Debug.Log($"{CompressedStream.Count} compressed Elements");   
    }

    private bool IsIdentical(UserInput input1, UserInput input2) {
        return Utilities.IsIdentical(input1.M, input2.M) && Utilities.IsIdentical(input1.R, input2.R) && input1.B == input2.B;
    }
}

[Serializable]
public class UserInputCounter {
    public UserInput U;
    public int C;
}

[Serializable]
public class InputStream {
    public List<UserInput> Stream = new List<UserInput>();
    public String username;
    public int score;
    public String userID;
}


[Serializable]
public class InputStreamUpload: InputStreamCompressed
{
    public static InputStreamUpload fromInputStream(InputStreamCompressed inputStream)
    {
        InputStreamUpload up = new InputStreamUpload();
        up.CompressedStream = inputStream.CompressedStream;
        up.username = inputStream.username;
        up.score = inputStream.score;
        return up;
    }
    public List<String> ghosts;
}
