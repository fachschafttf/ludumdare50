using System.Collections.Generic;
using System;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using Cinemachine;
using Utility;

public class PlayerPlayback : MonoBehaviour
{
    private List<Color> _ghostclours;
    private List<InputStream> _inputStreams;
    public static PlayerPlayback Instance;

    private List<GhostController> _currentGhosts;
    private List<Vector3> _ghostPos;
    public Transform player;
    public GhostPosAverage gpa;

    public GhostController GhostPrefab;
    public GameObject Camera;

    private int currentIteration;
    void Awake()
    {
        // Keep existing controller from different scene.
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        _inputStreams = new List<InputStream>();
        _currentGhosts = new List<GhostController>();
        _ghostPos = new List<Vector3>();
    }

    void Start()
    {
        _ghostclours = new List<Color>();
        _ghostclours.Add(Color.blue);
        _ghostclours.Add(Color.cyan);
        _ghostclours.Add(Color.green);
        _ghostclours.Add(Color.magenta);
        _ghostclours.Add(Color.red);
        FirebaseClient client = new FirebaseClient();
        IFirebaseQuery query = client.Get("CloudFunctions").Filter().OrderBy("score").LimitToLast("5").Build();
        StartCoroutine(query.ExecuteQuery(result =>
        {
            Debug.Log("Restoring Input Streams from Firebase");
            DeserializeInputStreamsFromJson(result);
            PlayAllExisting();
            FindObjectOfType<GameController>().Paused = false;
            FindObjectOfType<ToggleLoadingscreen>()?.toggle();
            GameController.Instance.GameInitializing = false;
        }));
    }

     void FixedUpdate() {
        _ghostPos = new List<Vector3>();
        for (var i = 0; i < _currentGhosts.Count; i++)
        {
            var currentInputStream = _inputStreams[i];
            if (currentIteration >= currentInputStream.Stream.Count)
            {
                continue;
            }
            var currentInput = currentInputStream.Stream[currentIteration];
            var factor = 1000;
            _currentGhosts[i].MovementControl.Movement = new Vector3(currentInput.M.x / factor, currentInput.M.y / factor, 0);
            _currentGhosts[i].MovementControl.Rotation = new Vector3(currentInput.R.x / factor, currentInput.R.y / factor, 0);

            _currentGhosts[i].ActionControl.ActionInBuffer = currentInput.B;
            _ghostPos.Add(_currentGhosts[i].gameObject.transform.position);
        }
        currentIteration++;         
        UpdateAverageGhostPosition();
    }

    public void PlayAllExisting() {
        currentIteration = 0;
        RemoveExistingGhosts();
        SpawnGhosts();
    }

    private void RemoveExistingGhosts() {
        for (var i = 0; i < _currentGhosts.Count; i++) {
            Destroy(_currentGhosts[i]);
        }
        _currentGhosts.Clear();
    }

    private void SpawnGhosts()
    {
        for (var i = 0; i < _inputStreams.Count; i++) {
            var ghost = Instantiate(GhostPrefab, new Vector3(0, 0, 0), Quaternion.identity);
            _currentGhosts.Add(ghost);
            ghost.Nametag.text = _inputStreams[i].username;
            ghost.Nametag.color = _ghostclours[i%_ghostclours.Count];
            ghost.GetComponent<SpriteRenderer>().color = _ghostclours[i%_ghostclours.Count];
            ghost.GetComponent<Light2D>().color = _ghostclours[i%_ghostclours.Count];
        }
    }

    public void AddInputStream(InputStream inputStream) {
        _inputStreams.Add(inputStream);
    }

    public void DeserializeInputStreamFromJson(string inputStreamAsJson)
    {
        var inputStream = JsonUtility.FromJson<InputStream>(inputStreamAsJson);
        _inputStreams.Add(inputStream);
    }

    public List<String> GetLoadedGhostNames()
    {
        List<String> names = new List<string>();
        foreach (InputStream stream in _inputStreams)
        {
           names.Add(stream.userID); 
        }
        return names;
    }
    
    public void DeserializeInputStreamsFromJson(string inputStreamsAsJson)
    {
        if (inputStreamsAsJson == "null") {
            return;
        }
        JObject googleSearch = JObject.Parse(inputStreamsAsJson);
        foreach (JToken child in googleSearch.Children())
        {
            InputStreamCompressed stream = child.First.ToObject<InputStreamCompressed>();
            var inputStream = new InputStream();
            inputStream.score = stream.score;
            inputStream.username = stream.username;
            inputStream.userID = stream.userID;
            foreach (var compressedUserInput  in stream.CompressedStream) {
                for(var i = 0; i < compressedUserInput.C; i++) {
                    inputStream.Stream.Add(compressedUserInput.U);
                }
            }
            _inputStreams.Add(inputStream);
        }
    }

    private void UpdateAverageGhostPosition()
    {
        Vector3 newPos = Vector3.zero;
        Vector3 playerPos = player.position;
        for (int i = 0; i < _ghostPos.Count; i++)
        {
            newPos = newPos + _ghostPos[i];
        }
        newPos = newPos / _ghostPos.Count;
        //not sure if needed
        //newPos.z = 0;
        float dist = Vector3.Distance(newPos, playerPos);
        Vector3 middle = playerPos + (0.5f * (newPos - playerPos));
        float zoomValue = 3f + dist* 0.2f;
        if (!float.IsNaN(newPos.x)) {
            gpa.SetPosition(newPos);
        }
        // change zoom value by script not tested yet 
        //To use uncomment and change follow in VCam to AverageGhost instead of Player
        //camera.GetComponent<CinemachineVirtualCamera>().m_Lens.OrthographicSize = 5f;
        
    }
}
