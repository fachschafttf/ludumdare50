using UnityEngine;

public class GameController : MonoBehaviour
{
    public int maxEnimies;
    public static GameController Instance;
    public int currentEnemies;

    public bool Paused;

    public int Score = 0;
    public string PlayerName;

    private GameObject _player;

    public bool GameInitializing = true;
    public bool GameOver = false;
    void Awake() {
        // Uncomment if you want to share between scenes.
        // DontDestroyOnLoad(gameObject);


        // Keep existing controller from different scene.

        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("NewGameManager");
        maxEnimies = 40;
        _player = GameObject.FindGameObjectWithTag("Player");
        PlayerPlayback.Instance?.PlayAllExisting();
        PlayerName = PlayerPrefs.GetString("PlayerName", "");

        Paused = GameObject.FindObjectOfType<PlayerPlayback>() != null;
    }

    // Update is called once per frame
    void Update()
    {
        CheckGameOver();
    }

    private void CheckGameOver()
    {
    }

    public void TriggerGameOver()
    {
        Paused = true;
        GameOver = true;
        GameObject.FindObjectOfType<Overlay>()?.GameOver();
    }
}
