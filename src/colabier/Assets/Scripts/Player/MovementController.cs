using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MovementController : MonoBehaviour
{


    public float MovementSpeed = 7f;
    public Vector3 Movement;
    public Vector3 Rotation;
    public Directions FaceDirection = Directions.North;
    public Vector3 currentMovement = Vector3.zero;
    
    // Start is called before the first frame update
     void Update()
    {
    }

    void  FixedUpdate() 
    {
        // Don't do stuff if Game Paused
        if (GameController.Instance?.Paused == true)
        {
            return;
        }
        Move();
        Rotate();
        currentMovement = Movement;
        Movement = new Vector3(0,0,0);
    }

    void Start() {
    }

    // Moves the camera relatively towards the view vector (W moves camera in direction forward of view perspective
    private void Move()
    {

        if (Movement.Equals(new Vector3(0,0,0)))
        {
            return;
        }

        float step = MovementSpeed * Time.deltaTime;

        
        // If need is there to move in faced direction instead of absolut
        // Rotate movement direction input vector, to match view direction of camera
        bool moveInFacedDirection = false;
        Vector3 movementDirection = moveInFacedDirection ? Quaternion.Euler(0, transform.eulerAngles.y, 0) * Movement : Movement;
        movementDirection.y *= 0.5f;


        transform.position = Vector3.MoveTowards(transform.position, transform.position + movementDirection, step * movementDirection.magnitude);
        // ArrowGizmo.ForGizmo();
    }



    private void Rotate()
    {
        if( Rotation.Equals(new Vector3(0,0,0)))
        {
            return;
        }

        FaceDirection = Utilities.TransformVectorInDirection(Rotation);

        //Not needed anymore. Sprites are changed instead of character rotated.

        
        // var rotation = transform.eulerAngles;
        
        // rotation.z = (int)FaceDirection * 45 + 90;

        // transform.eulerAngles = rotation;
    }

    public void OnDrawGizmos()
    {
        ForGizmo(transform.position, currentMovement);
    }

    public static void ForGizmo(Vector3 pos, Vector3 direction, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f)
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(pos, direction);
    }

}
