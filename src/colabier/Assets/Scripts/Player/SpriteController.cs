using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteController : MonoBehaviour
{
    public bool DrawTransparent;
    private SpriteRenderer _renderer;
    private MovementController _movement;
    private Animator _animator;
    private string _currentState;
    private List<string> _idleAnimations = new List<string>();
    private List<string> _walkAnimations = new List<string>();
    // Start is called before the first frame update
    void Start()
    {
        _renderer = GetComponent<SpriteRenderer>();
        if (DrawTransparent) {
            AdjustAlpha();
        }
        _movement = GetComponent<MovementController>();
        _animator = GetComponent<Animator>();

        foreach (AnimationClip animation in _animator.runtimeAnimatorController.animationClips)
        {
            if (animation.name.Contains("idle"))
            {
                _idleAnimations.Add(animation.name);
            }
            if (animation.name.Contains("walk"))
            {
                _walkAnimations.Add(animation.name);
            }
        }
    }

    private void AdjustAlpha(){
        AdjustAlphaForRenderer(_renderer);
        var renderers = GetComponentsInChildren<SpriteRenderer>();
        foreach(var renderer in renderers) {
            AdjustAlphaForRenderer(renderer);
        }

    }

    private void AdjustAlphaForRenderer(SpriteRenderer renderer) {
        var color = renderer.color;
        color.a = 0.5f;
        renderer.color = color;
    }


    void FixedUpdate() {
        var direction = _movement.FaceDirection;
        Vector3 movementSpeed = _movement.currentMovement;

        if (movementSpeed.sqrMagnitude > 0)
        {
            int targetIndex = Mathf.Min(_walkAnimations.Count-1, Utilities.Mod(-3 + (int)direction, 8));
            ChangeAnimationState(_walkAnimations[targetIndex]);
        } else
        {
            int targetIndex = Mathf.Min(_idleAnimations.Count-1, Utilities.Mod(-3 + (int)direction, 8));
            ChangeAnimationState(_idleAnimations[targetIndex]);
        }
    }

    private void ChangeAnimationState(string newState)
    {
        if (_currentState == newState) return;

        _animator.Play(newState);

        _currentState = newState;
    }
}
