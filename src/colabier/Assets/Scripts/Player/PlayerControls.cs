using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerControls : MonoBehaviour, MainInput.IAbsolutPlayerActions, MainInput.IInputDetectionActions
{
    private MainInput _controls;

    private Vector3 _movement;
    private Vector3 _rotation;

    private bool _actionInBuffer;


    private GameObject _player;
    private MovementController _playerMovementController;
    private ActionController _playerActionController;



    private InputDevices CurrentInputDevice;


    // Needed for input system
    // ReSharper disable once UnusedMember.Local
    private void OnEnable()
    {
        if (_controls == null)
        {
            _controls = new MainInput();
            _controls.AbsolutPlayer.SetCallbacks(this);
            _controls.InputDetection.SetCallbacks(this);
        }
        _controls.Enable();
    }

    // Needed for input system
    // ReSharper disable once UnusedMember.Local
    private void OnDisable()
    {
        _controls.Disable();
    }

    void Update() {
        EvaluateRotationFromMousePosition();
    }



    // Start is called before the first frame update
    void Start()
    {
        _playerMovementController = GetComponent<MovementController>();
        _playerActionController = GetComponent<ActionController>();
    }


    void  FixedUpdate() {
        if (GameController.Instance?.Paused == true)
        {
            return;
        }   
        PlayerRecorder.Instance?.RegisterPlayerUpdate(_movement, _rotation, _actionInBuffer); 
        _playerMovementController.Movement = _movement;
        _playerMovementController.Rotation = _rotation;
        _playerActionController.ActionInBuffer = _actionInBuffer;

    }   

    private void EvaluateRotationFromMousePosition()
    {
        if (CurrentInputDevice == InputDevices.Gamepad) {
            return;
        }
        var mousePosition = Mouse.current.position.ReadValue();
        var worldPosition = Camera.main.ScreenToWorldPoint(new Vector3(mousePosition.x, mousePosition.y, 0));
        worldPosition.z=transform.position.z;
        
        _rotation = Quaternion.Euler(0, 0, 45/2) * (worldPosition - transform.position).normalized;
    }

    // Grab Movement input from input system.
    public void OnPlayerMovement(InputAction.CallbackContext context)
    {
        if (GameController.Instance?.Paused == true)
        {
            return;
        }

        // Possible with Vector2
        Vector2 direction = context.ReadValue<Vector2>();
        _movement = new Vector3(direction.x, direction.y, 0);
    }

    

    // Grab Movement input from input system.
    public void OnPlayerRotation(InputAction.CallbackContext context)
    {
        if (GameController.Instance?.Paused == true || CurrentInputDevice == InputDevices.KeyboardAndMouse)
        {
            return;
        }

        // Possible with Vector2
        Vector2 direction = context.ReadValue<Vector2>();

        _rotation = new Vector3(direction.x, direction.y, 0);
    }

    // Grab Movement input from input system.
    public void OnPlayerAction(InputAction.CallbackContext context)
    { 
        // Prevent double execution.
        if (GameController.Instance?.Paused == true)
        {
            return;
        }
        if (context.performed) {
            _actionInBuffer = true;

        }
        else if (context.canceled) {
            _actionInBuffer = false;
        }

    }
    
    public void OnKeyboardUsed (InputAction.CallbackContext context)
    { 
        CurrentInputDevice = InputDevices.KeyboardAndMouse;
    
    }

    public void OnMouseUsed(InputAction.CallbackContext context)
    {
        CurrentInputDevice = InputDevices.KeyboardAndMouse;
    }

    public void OnGamepadUsed (InputAction.CallbackContext context)
    {
        CurrentInputDevice = InputDevices.Gamepad;
    }


}
