using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuPlayerController : MonoBehaviour
{
    public Vector3 Rotation;
    public Vector3 Movement;
    public Directions FaceDirection;

    private MovementController _movement;
    // Start is called before the first frame update
    void Start()
    {
        _movement = GetComponent<MovementController>();
        
    }

    void Update() {
        _movement.Movement = Movement;
        _movement.Rotation = Rotation;
        _movement.FaceDirection = FaceDirection;
    }

}
