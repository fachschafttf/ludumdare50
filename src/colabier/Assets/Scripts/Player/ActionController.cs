using Attacks.Projectiles;
using UnityEngine;

public class ActionController : MonoBehaviour
{
    public bool Range = true;
    public bool ActionInBuffer;
    private MovementController _player;
    public MeeleActionController Sword;
    public ProjectileFactory Gun;

    // Start is called before the first frame update
    void Start()
    {
        if (Sword == null) {
            Debug.LogWarning("ActionController has no Sword Weapon");
        }
        if (Gun == null) {
            Debug.LogWarning("ActionController has no Gun Weapon");
        }
        _player = GetComponent<MovementController>();
    }

    void FixedUpdate()
    {
        // Don't do stuff if Game Paused
        if (GameController.Instance?.Paused == true)
        {
            return;
        }
        UpdateWeapons();
        ExecuteAction();
    }


    // This is ultra ugly, but historicly grew like that.
    private void UpdateWeapons() {
        Gun?.gameObject.SetActive(Range ? true : false);
        Sword?.gameObject.SetActive(Range ? false: true);
        var angle = Quaternion.FromToRotation(Vector3.up, _player.Rotation).eulerAngles.z + 45 * 1.5f;
        var weaponRotation = new Vector3(45, 0, angle);

        if (Range && Gun) {
            Gun.gameObject.transform.eulerAngles = weaponRotation;
        }
        else if (!Range && Sword) {            
            Sword.gameObject.transform.eulerAngles = weaponRotation;
        }
    }
    
    private void ExecuteAction() {
        if (!ActionInBuffer) {
            return;
        }

        if (Range)
        {
            var bullet =Gun?.Shoot();
            if (bullet) {
                bullet.GetComponent<StandartProjectile>().Controller = gameObject;
            }
        }
        else
        {
            Sword?.Slash();
        }
        
        ActionInBuffer = false;
    }
}
