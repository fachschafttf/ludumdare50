using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour, IDamageable
{
    [Header("HP")]
    public float MaxHP;
    public float CurrentHP;


    private bool _dead;

    void Start() {
        CurrentHP = MaxHP;
    }

    public void ApplyDamage(float damage = 10f)
    {
        if (_dead) {
            return;
        }
        CurrentHP -= damage;

        if (CurrentHP <= 0f)
        {
            _dead = true;
            CurrentHP = 0f;
            Die();
        }
        else
        {
            if (gameObject.tag == "Player") {
                AudioController.Instance?.PlaySound("HitPlayer");
            }
            else {
                var ghostController = GetComponent<GhostController>();
                AudioController.Instance?.PlaySoundRelativeToDistance("HitPlayer", ghostController.DistanceToPlayer());

            }
            // Damage functionality
        }
    }

    public void Die()
    {
        // Death functionality
        if (gameObject.tag == "Player") {
            GameController.Instance?.TriggerGameOver();
        }
        else {

            gameObject.SetActive(false);
        }
        // we might need to wait before destroying the object immediately (playing animation or smth)
    }
}
