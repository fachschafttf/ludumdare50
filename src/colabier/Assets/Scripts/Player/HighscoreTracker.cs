using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighscoreTracker : MonoBehaviour
{
    public int ScorePerUbdate;
    public int ScorePerHit;
    public int PersonalScore;
    // Start is called before the first frame update
    void Start()
    {
        PersonalScore = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameController.Instance.Paused)
        {
            Invoke("Up",1);
        }
    }

    private void Up()
    {
        PersonalScore += ScorePerUbdate;
        GameController.Instance.Score += ScorePerUbdate;
    }
    public void hit()
    {
        //Debug.Log("Hit");
        PersonalScore += ScorePerHit;
        GameController.Instance.Score += ScorePerHit;
    }
}
