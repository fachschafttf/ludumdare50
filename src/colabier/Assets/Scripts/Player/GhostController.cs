using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GhostController : MonoBehaviour
{
    public TextMeshPro Nametag;
    public MovementController MovementControl;
    public ActionController ActionControl;

    private GameObject _player;
    // Start is called before the first frame update
    void Awake()
    {
        MovementControl = GetComponent<MovementController>();
        ActionControl = GetComponent<ActionController>();
    }

    void Start() {
        _player = GameObject.FindGameObjectWithTag("Player");
    }

    public float DistanceToPlayer() {
        return Mathf.Max(0.01f, (transform.position - _player.transform.position).magnitude);
    }

}
