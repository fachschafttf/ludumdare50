using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;

namespace Utility
{
    public class AnonymousToken
    {
        public String kind { get; set; }
        public String idToken { get; set; }
        public String refreshToken { get; set; }
        public String expiresIn { get; set; }
        public string localId { get; set; }

        public void RefreshWith(RefreshToken refreshToken)
        {
            idToken = refreshToken.id_token;
            this.refreshToken = refreshToken.refresh_token;
            expiresIn = refreshToken.expires_in;
            localId = refreshToken.user_id;
        }
    }
    
    public class RefreshToken
    {
        public String access_token { get; set; }
        public String expires_in { get; set; }
        public String refresh_token { get; set; }
        public String id_token { get; set; }
        public String user_id  { get; set; }
        public string project_id { get; set; }
    }


    public class AnonymousSession
    {
        private static string SCHEME = "https";
        private static string API_KEY = "AIzaSyBBM2wMWHt5zVWAwV7i-yKvyGPSEaTDwl0";
        private static string QUERY = $"key={API_KEY}";

        private static string HOST_TOKEN = "identitytoolkit.googleapis.com";
        private static string PATH_TOKEN = "/v1/accounts:signUp";

        private static Uri URI_TOKEN = new UriBuilder
        {
            Scheme = SCHEME,
            Host = HOST_TOKEN,
            Path = PATH_TOKEN,
            Query = QUERY
        }.Uri;

        private static string HOST_REFRESH = "securetoken.googleapis.com";
        private static string PATH_REFRESH = "/v1/token";

        private static Uri URI_REFRESH = new UriBuilder
        {
            Scheme = SCHEME,
            Host = HOST_REFRESH,
            Path = PATH_REFRESH,
            Query = QUERY
        }.Uri;

        #nullable enable
        public static IEnumerator GetAnonymousToken(Action<AnonymousToken> callback)
        {
            String sessionPref = PlayerPrefs.GetString("session", "!");

            if (sessionPref == "!")
            {
                yield return FetchNewAnonymousToken(newToken =>
                {
                    PlayerPrefs.SetString("session", newToken);
                    Debug.Log("Fetched new Session");
                    AnonymousToken? token = JsonConvert.DeserializeObject<AnonymousToken>(newToken);
                    if (token == null)
                    {
                        Debug.LogError("Failed parsing anonymous token.");
                        throw new InvalidCastException();
                    }

                    callback(token);
                });
            }
            else
            {
                AnonymousToken? token = JsonConvert.DeserializeObject<AnonymousToken>(sessionPref);
                if (token != null)
                {
                    Debug.Log("Loaded Session From Storage");
                }
                else
                {
                    throw new Exception("Failed to load Session from Storage");
                }

                callback(token);

                //TODO: only refresh token when it is too old -> catch errors in api response
                yield return RefreshToken(token, newToken =>
                {
                    Debug.Log("Refreshed Token");
                    RefreshToken? refreshToken = JsonConvert.DeserializeObject<RefreshToken>(newToken);
                    token.RefreshWith(refreshToken);
                    PlayerPrefs.SetString("session", JsonConvert.SerializeObject(token));
                    callback(token);
                });
            }
        }

        public static IEnumerator FetchNewAnonymousToken(Action<String> callback)
        {
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes("{\"returnSecureToken\":\"true\"}");
            UploadHandlerRaw uploadHandler = new UploadHandlerRaw(bytes);
            uploadHandler.contentType = " application/json";
            using (UnityWebRequest request = new UnityWebRequest(URI_TOKEN, "POST"))
            {
                request.uploadHandler = uploadHandler;
                request.downloadHandler = new DownloadHandlerBuffer();
                request.SetRequestHeader("X-Client-Version", "");
                request.SetRequestHeader("X-Firebase-Client", "");
                request.SetRequestHeader("X-Firebase-gmpid", "");
                yield return request.SendWebRequest();
                if (request.result != UnityWebRequest.Result.Success)
                {
                    FirebaseErrorResponse error = FirebaseErrorResponse.FromJson(request.downloadHandler.text);
                    error.RaiseException();
                }
                else
                {
                    Debug.Log(request.downloadHandler.text);
                    callback(request.downloadHandler.text);
                }
            }
        }

        public static IEnumerator RefreshToken(AnonymousToken token, Action<String> callback)
        {
            byte[] bytes = UnityWebRequest.SerializeSimpleForm(new Dictionary<string, string>()
            {
                {"grant_type", "refresh_token"},
                {"refresh_token", token.refreshToken}
            });
            using (UnityWebRequest request = new UnityWebRequest(URI_REFRESH, "POST"))
            {
                using (UploadHandlerRaw uploadHandler = new UploadHandlerRaw(bytes))
                {


//            UnityWebRequest request = new UnityWebRequest(URI_REFRESH, "POST");
//            UploadHandlerRaw uploadHandler = new UploadHandlerRaw(bytes);
                    uploadHandler.contentType = "application/x-www-form-urlencoded";
                    request.uploadHandler = uploadHandler;
                    request.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    request.downloadHandler = new DownloadHandlerBuffer();
                    yield return request.SendWebRequest();
                    if (request.result != UnityWebRequest.Result.Success)
                    {
                        FirebaseErrorResponse error = FirebaseErrorResponse.FromJson(request.downloadHandler.text);
                        error.RaiseException();
                    }
                    else
                    {
                        callback(request.downloadHandler.text);
                    }
                }
            }
        }
    }
}