﻿using System;
using Newtonsoft.Json.Linq;

namespace Utility
{
    public class FirebaseApiError
    {
        public String error { get; set; }
        public long statusCode { get; set; }

        public FirebaseApiError(String responseJson, long statusCode)
        {
            this.statusCode = statusCode;
            this.error = JObject.Parse(responseJson)["error"].ToString();
        }

        public FirebaseApiErrors Type()
        {
            switch (error)
            {
                case "Permission denied":
                    return FirebaseApiErrors.PERMISSION_DENIED;
                case "Could not parse auth token.":
                    return FirebaseApiErrors.COULD_NOT_PARSE_AUTH_TOKEN;
                case "Token to old": //TODO: replace with correct value
                    return FirebaseApiErrors.TOKEN_TOO_OLD;
                default:
                    return FirebaseApiErrors.UNKNOWN;
            }
        }

        public void RaiseException()
        {
            switch (Type())
            {
               case FirebaseApiErrors.PERMISSION_DENIED:
                   throw new PermissionDenied();
               case FirebaseApiErrors.COULD_NOT_PARSE_AUTH_TOKEN:
                   throw new CouldNotParseAuthToken();
               case FirebaseApiErrors.TOKEN_TOO_OLD:
                   throw new TokenTooOld();
               case FirebaseApiErrors.UNKNOWN:
                   throw new FirebaseApiException(); //TODO: implement Unknown exception for this group
            }
        }
    }

    public class FirebaseApiException : Exception { }
    public class CouldNotParseAuthToken: FirebaseApiException { }
    public class TokenTooOld: FirebaseApiException { }
    public class PermissionDenied: FirebaseApiException { }
//    public class Unknown: FirebaseApiException { }

    public enum FirebaseApiErrors
    {
        COULD_NOT_PARSE_AUTH_TOKEN,
        TOKEN_TOO_OLD,
        PERMISSION_DENIED,
        UNKNOWN
    }
}