
using System;
using UnityEngine;
// Static function for stuff like Modulo, or simple math things

// ReSharper disable once CheckNamespace
public static class Utilities
{
    public static int Mod(int x, int m)
    {
        return (x % m + m) % m;
    }

    public static Vector2 RotateVector2(Vector2 vector, float degree)
    {
        var tempVector = new Vector3(vector.x, 0, vector.y);
        tempVector = Quaternion.AngleAxis(degree, Vector3.up) * tempVector;
        return new Vector2(tempVector.x, tempVector.z);
    }

    public static bool IsAnyCoordinateTooFar(Vector2 vector, float value)
    {
        return Math.Abs(vector.x) > value || Math.Abs(vector.y) > value;
    }


    public static Directions TransformVectorInDirection(Vector3 vector)
    {
        // Calculate Angle of Vector in relation to Up-Axis
        var angle = Quaternion.FromToRotation(Vector3.up, vector).eulerAngles.z;

        // Calculate Index in Enum
        var directionIndex = (int)angle/45f;
        
        return (Directions)directionIndex;

    }

    public static Vector3 RoundVector3(Vector3 vector, int decimals) {
        var factor = Mathf.Pow(10, decimals);
        return new Vector3(Mathf.Round(vector.x * factor)/factor, Mathf.Round(vector.y * factor)/factor,Mathf.Round(vector.z * factor));
    }

    public static bool IsIdentical(Vector2Int v1, Vector2Int v2) {
        return v1.x == v2.x && v1.y == v2.y;
    }

    

}
