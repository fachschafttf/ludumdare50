using UnityEngine;

namespace Utility
{
    public class Firebase : MonoBehaviour
    {
        void Start()
        {
            Debug.Log("Start");
            FirebaseClient client = new FirebaseClient();
            IFirebaseQuery query = client.Get("GameObjects").Filter().OrderBy("Score").StartAt("10").Build();
            StartCoroutine(query.ExecuteQuery(result => Debug.Log($"Received: {result}")));

//            FirebaseBuilderMethodData patch = client.Patch("GameObjects");
//            FirebaseBuilderMethodData post = client.Post("GameObjects");
//            FirebaseBuilderMethodData put = client.Put("Test").Child("123");
//            FirebaseBuilderMethodEmpty delete = client.Delete("GameObjects").Child("123");
//            StartCoroutine(patch.Execute($"{{\"Test\":{{\"Score\": {Random.Range(5, 100)}}}}}"));
//            StartCoroutine(post.Execute($"{{\"Score\": {Random.Range(5, 100)},\"birthday\": \"June 23, 1912\"}}"));
//            StartCoroutine(put.Execute($"{{\"Score\": {Random.Range(5, 100)},\"birthday\": \"June 23, 1912\"}}"));
//            StartCoroutine(delete.Execute());
        }

        private void Update()
        {
        }
    }
}