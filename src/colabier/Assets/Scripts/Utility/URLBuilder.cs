using System;

namespace Utility
{
    public class URLBuilder
    {
        private static string SCHEME = "https";
        private static string HOST = "ld50-f06b9-default-rtdb.europe-west1.firebasedatabase.app";

        public static Uri BuildUri(String auth, String path)
        {
            UriBuilder builder = new UriBuilder
            {
                Scheme = SCHEME,
                Host = HOST,
                Path = $"{path}.json",
                Query = $"timeout=5s&auth={auth}"
            };
            return builder.Uri;
        }

        public static Uri BuildUri(String auth, String path, String query)
        {
            UriBuilder builder = new UriBuilder
            {
                Scheme = SCHEME,
                Host = HOST,
                Path = $"{path}.json",
                Query = $"timeout=5s&auth={auth}&{query}"
            };
            return builder.Uri;
        }
    }
}