//public enum Directions {North, NorthEast, East, SouthEast, South, SouthWest, West, NorthWest}

public enum Directions {North, NorthWest, West, SouthWest, South, SouthEast, East, NorthEast}

public enum InputDevices {KeyboardAndMouse, Gamepad}