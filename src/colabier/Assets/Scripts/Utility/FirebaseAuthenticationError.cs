﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Utility
{
    public class FirebaseAuthError
    {
        public String domain { get; set; }
        public String reason { get; set; }
        public String message { get; set; }
    }

    public class FirebaseErrorResponse
    {
        public List<FirebaseAuthError> errors { get; set; }
        public int code { get; set; }
        public String message { get; set; }

        public static FirebaseErrorResponse FromJson(String json)
        {
            return JObject.Parse(json)["error"]!.ToObject<FirebaseErrorResponse>();
        }

        public Error Type()
        {
            switch (message)
            {
                case "TOKEN_EXPIRED":
                    return Error.TOKEN_EXPIRED;
                case "INVALID_REFRESH_TOKEN":
                    return Error.INVALID_REFRESH_TOKEN;
                case "API key not valid. Please pass a valid API key.":
                    return Error.API_KEY_NOT_VALID;
                default:
                    return Error.UNKNOWN;
            }
        }

        public void RaiseException()
        {
            switch (Type())
            {
                case Error.TOKEN_EXPIRED:
                    throw new TokenExpiredException();
                case Error.INVALID_REFRESH_TOKEN:
                    throw new InvalidRefreshToken();
                case Error.API_KEY_NOT_VALID:
                    throw new APIKeyNotValid();
                default:
                    throw new Unknown();
            }
        }
    }

    public class FirebaseAuthException : Exception { }
    public class TokenExpiredException : FirebaseAuthException { }
    public class APIKeyNotValid : FirebaseAuthException { }
    public class InvalidRefreshToken : FirebaseAuthException { }
    public class Unknown : FirebaseAuthException { }


    public enum Error
    {
        TOKEN_EXPIRED,
        API_KEY_NOT_VALID,
        INVALID_REFRESH_TOKEN,
        MISSING_REFRESH_TOKEN,
        UNKNOWN
    }
}