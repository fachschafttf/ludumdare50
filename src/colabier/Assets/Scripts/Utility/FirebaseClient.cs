using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;

namespace Utility
{
    public class FirebaseClient
    {
        public AnonymousToken token;
        private bool InitInProgress;

        public FirebaseClient()
        {
        }

        public IEnumerator Initialize()
        {
            //TODO: load token from disk and possibly refresh
            if (InitInProgress)
            {
                Debug.LogWarning("Initialize already in progress. Waiting...");
                yield return new WaitUntil(() => !this.InitInProgress);
            }
            else if (this.token == null)
            {
                this.InitInProgress = true;
                yield return AnonymousSession.GetAnonymousToken(returnValue =>
                {
                    this.token = returnValue;
                    this.InitInProgress = false;
                    Debug.Log($"Client Initialized");
                });
            }
        }

        public IEnumerator RefreshToken(Action callback)
        {
            yield return AnonymousSession.RefreshToken(this.token, newToken =>
            {
                RefreshToken? refreshToken = JsonConvert.DeserializeObject<RefreshToken>(newToken);
                token.RefreshWith(refreshToken);
                PlayerPrefs.SetString("session", JsonConvert.SerializeObject(token));
                callback();
            });
        }

        protected internal String GetToken()
        {
            return token.idToken;
        }

        public FirebaseQueryBuilder Get(String path)
        {
            return new FirebaseQueryBuilder(this, path);
        }

        public FirebaseBuilderMethodData Patch(String path)
        {
            return new FirebaseBuilderMethodData(this, "PATCH").Child(path);
        }

        public FirebaseBuilderMethodData Put(String path)
        {
            return new FirebaseBuilderMethodData(this, "PUT").Child(path);
        }

        public FirebaseBuilderMethodData Post(String path)
        {
            return new FirebaseBuilderMethodData(this, "POST").Child(path);
        }

        public FirebaseBuilderMethodEmpty Delete(String path)
        {
            return new FirebaseBuilderMethodEmpty(this, "DELETE").Child(path);
        }
    }

    public interface PathSegment
    {
        public string Build(FirebaseClient client);
    }

    public class ChildPathSegment : PathSegment
    {
        private String path;
        public ChildPathSegment(String path)
        {
            this.path = path;
        }
        public string Build(FirebaseClient client)
        {
            return this.path;
        }
    }

    public class UidPathSegment : PathSegment
    {
        public string Build(FirebaseClient client)
        {
            return client.token.localId;
        }
    }

    public abstract class FirebaseBuilderMethod
    {
        protected FirebaseClient client { get; set; }
        protected List<PathSegment> pathSegments;
        protected String method { get; set; }

        protected internal FirebaseBuilderMethod(FirebaseClient client, String method)
        {
            this.client = client;
            this.pathSegments = new List<PathSegment>();
            this.method = method;
        }

        protected internal FirebaseBuilderMethod(FirebaseBuilderMethod builder)
        {
            this.client = builder.client;
            this.method = builder.method;
        }
        
        protected String buildPath()
        {
            List<String> segmentStrings = new List<string>();
            foreach (PathSegment segment in pathSegments)
            {
                segmentStrings.Add(segment.Build(client));
            }
            String nPath = String.Join("/", segmentStrings);
            return nPath;
        }
        
    }

    public class FirebaseBuilderMethodEmpty : FirebaseBuilderMethod
    {
        protected internal FirebaseBuilderMethodEmpty(FirebaseClient client, string method) : base(client,
            method)
        {
        }

        protected internal FirebaseBuilderMethodEmpty(FirebaseBuilderMethod builder) : base(builder)
        {
        }
        
        public FirebaseBuilderMethodEmpty Child(String child)
        {
            pathSegments.Add(new ChildPathSegment(child));
            return this;
        }
        
        public FirebaseBuilderMethodEmpty Uid()
        {
            pathSegments.Add(new UidPathSegment());
            return this;
        }
        

        public IEnumerator Execute()
        {
            yield return client.Initialize();
            String nPath = buildPath();
            
            Uri uri = URLBuilder.BuildUri(client.GetToken(), nPath, "print=silent");
            using (UnityWebRequest request = new UnityWebRequest(uri, this.method))
            {
                yield return request.SendWebRequest();

                if (request.result != UnityWebRequest.Result.Success)
                {
                    Debug.LogError($"Error while executign Method {this.method}: {request.error}\n{request.downloadHandler.text}");
                }
            }
        }
    }

    public class FirebaseBuilderMethodData : FirebaseBuilderMethod
    {
        protected internal FirebaseBuilderMethodData(FirebaseClient client, string method) : base(client,
            method)
        {
        }

        protected internal FirebaseBuilderMethodData(FirebaseBuilderMethod builder) : base(builder)
        {
        }
        
        public FirebaseBuilderMethodData Child(String child)
        {
            pathSegments.Add(new ChildPathSegment(child));
            return this;
        }
        
        public FirebaseBuilderMethodData Uid()
        {
            pathSegments.Add(new UidPathSegment());
            return this;
        }
        

        public IEnumerator Execute(String json)
        {
            yield return client.Initialize();
            String nPath = buildPath();
            Uri uri = URLBuilder.BuildUri(client.GetToken(), nPath, "print=silent");
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(json);
            using (UploadHandlerRaw uploadHandler = new UploadHandlerRaw(bytes))
            {
                uploadHandler.contentType = " application/json";
                using (UnityWebRequest request = new UnityWebRequest(uri, this.method))
                {
                    request.uploadHandler = uploadHandler;
                    yield return request.SendWebRequest();

                    if (request.result != UnityWebRequest.Result.Success)
                    {
                        Debug.LogError($"Error while executing Method {this.method}: {request.error}");
                    }
                }
            }
        }
    }

    public interface IFirebaseQueryBuilder
    {
        public IFirebaseQuery Build();
    }

    public interface IFirebaseQuery
    {
        public IEnumerator ExecuteQuery(Action<String> callback);
        public Uri BuildUri();
    }

    public abstract class AFirebaseQuery : IFirebaseQuery
    {
        protected FirebaseClient client;

        public IEnumerator ExecuteQuery(Action<String> callback)
        {
            yield return client.Initialize();
            using (UnityWebRequest www = UnityWebRequest.Get(BuildUri()))
            {
//            UnityWebRequest www = UnityWebRequest.Get(BuildUri());
                yield return www.SendWebRequest();

                if (www.result != UnityWebRequest.Result.Success)
                {
                    FirebaseApiError error = new FirebaseApiError(www.downloadHandler.text, www.responseCode);
                    //TODO: check error for TOO_OLD and then do the next 2 lines
                    //Old token: eyJhbGciOiJSUzI1NiIsImtpZCI6IjZhNGY4N2ZmNWQ5M2ZhNmVhMDNlNWM2ZTg4ZWVhMGFjZDJhMjMyYTkiLCJ0eXAiOiJKV1QifQ.eyJwcm92aWRlcl9pZCI6ImFub255bW91cyIsImlzcyI6Imh0dHBzOi8vc2VjdXJldG9rZW4uZ29vZ2xlLmNvbS9sZDUwLWYwNmI5IiwiYXVkIjoibGQ1MC1mMDZiOSIsImF1dGhfdGltZSI6MTY0OTAyMjI3MiwidXNlcl9pZCI6IlpCYnVMcnk0Um5aaWs1Sjl0OHFhZlJUd1lGMDIiLCJzdWIiOiJaQmJ1THJ5NFJuWmlrNUo5dDhxYWZSVHdZRjAyIiwiaWF0IjoxNjQ5MDIyMjcyLCJleHAiOjE2NDkwMjU4NzIsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnt9LCJzaWduX2luX3Byb3ZpZGVyIjoiYW5vbnltb3VzIn19.iFUwqhl5EYic1lbjuUocbK6-5v9wvJHqqhcjOQ0cWsmabp595_gNBhUBQYdDrY17WId4vTibhPhUbpoJxkhKCmtBlSfaXhbp5STzyqHbRCuDjkOvoC3T9Rub2T3DSi8frp_BpzbPRDcLWg7GwhojjJ7kesJCXw1V4UqXLs6Dwz9d2O-nLqDvYDvzWFz8qhNQes3iRPxZiIC1HUBVa6ek7N5sfq037AIf4KaNaRSnqVENezcCNPIPm9XxiuEgJbH3DC55583WM9adOTP_VpmQ1D3m-v8iznlBXSFjPcSWKkdNAvKQ7KTOoA4Q0jue9AXnBu2iy8dN4IlOr6hRpZJuhQ
                    /*
                     {
        "kind": "identitytoolkit#SignupNewUserResponse",
        "idToken": "eyJhbGciOiJSUzI1NiIsImtpZCI6IjZhNGY4N2ZmNWQ5M2ZhNmVhMDNlNWM2ZTg4ZWVhMGFjZDJhMjMyYTkiLCJ0eXAiOiJKV1QifQ.eyJwcm92aWRlcl9pZCI6ImFub255bW91cyIsImlzcyI6Imh0dHBzOi8vc2VjdXJldG9rZW4uZ29vZ2xlLmNvbS9sZDUwLWYwNmI5IiwiYXVkIjoibGQ1MC1mMDZiOSIsImF1dGhfdGltZSI6MTY0OTAyMjI3MiwidXNlcl9pZCI6IlpCYnVMcnk0Um5aaWs1Sjl0OHFhZlJUd1lGMDIiLCJzdWIiOiJaQmJ1THJ5NFJuWmlrNUo5dDhxYWZSVHdZRjAyIiwiaWF0IjoxNjQ5MDIyMjcyLCJleHAiOjE2NDkwMjU4NzIsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnt9LCJzaWduX2luX3Byb3ZpZGVyIjoiYW5vbnltb3VzIn19.iFUwqhl5EYic1lbjuUocbK6-5v9wvJHqqhcjOQ0cWsmabp595_gNBhUBQYdDrY17WId4vTibhPhUbpoJxkhKCmtBlSfaXhbp5STzyqHbRCuDjkOvoC3T9Rub2T3DSi8frp_BpzbPRDcLWg7GwhojjJ7kesJCXw1V4UqXLs6Dwz9d2O-nLqDvYDvzWFz8qhNQes3iRPxZiIC1HUBVa6ek7N5sfq037AIf4KaNaRSnqVENezcCNPIPm9XxiuEgJbH3DC55583WM9adOTP_VpmQ1D3m-v8iznlBXSFjPcSWKkdNAvKQ7KTOoA4Q0jue9AXnBu2iy8dN4IlOr6hRpZJuhQ",
        "refreshToken": "AIwUaOnQq2KV81zcmxoeQqbQNuimA9KYKPzvb-6Lcb5PmsAs874yE1CFs5VjhZS9EaQIgE9F0U3fVzsD1t0wuK26zGWAmRgX4ZR7W1CTtx1fT_Vy8OKPllUAEQ72TRqunvtibuuTJUElnEkluB671we1NPrtv-AUsZWGlSGrve5V_tks3zkQh4Y",
        "expiresIn": "3600",
        "localId": "ZBbuLry4RnZik5J9t8qafRTwYF02"
    }
                     */
                    yield return client.RefreshToken(() => { });
                    yield return ExecuteQuery(callback);
                    error.RaiseException();
                }
                else
                {
                    callback(www.downloadHandler.text);
                }
            }
        }

        public abstract Uri BuildUri();
    }

    public class FirebaseQuery : AFirebaseQuery
    {
        private String path;

        protected internal FirebaseQuery(FirebaseClient client, String path)
        {
            this.client = client;
            this.path = path;
        }

        public override Uri BuildUri()
        {
            return URLBuilder.BuildUri(client.GetToken(), path);
        }
    }

    public class FilterFirebaseQuery : AFirebaseQuery
    {
        private String path;
        private String query;

        protected internal FilterFirebaseQuery(FirebaseClient client, String path, String query)
        {
            this.client = client;
            this.path = path;
            this.query = query;
        }

        public override Uri BuildUri()
        {
            return URLBuilder.BuildUri(client.GetToken(), path, query);
        }
    }

    public class FirebaseQueryBuilder : IFirebaseQueryBuilder
    {
        private FirebaseClient client;
        private String path;

        protected internal FirebaseQueryBuilder(FirebaseClient client, String path)
        {
            //TODO: token assumed to be valid. throw error if token times out
            this.client = client;
            this.path = path;
        }

        public FirebaseQueryBuilder Child(String path)
        {
            return new FirebaseQueryBuilder(client, $"{this.path}/{path}");
        }

        public FirebaseFilterQueryBuilder Filter()
        {
            return new FirebaseFilterQueryBuilder()
            {
                client = client,
                path = path,
                orderBy = "",
                startAt = "",
                endAt = "",
                equalTo = "",
                limitToFirst = "",
                limitToLast = ""
            };
        }

        public IFirebaseQuery Build()
        {
            return new FirebaseQuery(client, path);
        }
    }

    public class FirebaseFilterQueryBuilder : IFirebaseQueryBuilder
    {
        protected internal FirebaseClient client { get; set; }
        protected internal String path { get; set; }
        protected internal String orderBy { get; set; }
        protected internal String startAt { get; set; }
        protected internal String endAt { get; set; }

        protected internal String equalTo { get; set; }

        //TODO: make mutally exclusive
        protected internal String limitToFirst { get; set; }
        protected internal String limitToLast { get; set; }

        protected internal FirebaseFilterQueryBuilder()
        {
            //TODO: token assumed to be valid. throw error if token times out
        }

        protected internal FirebaseFilterQueryBuilder(FirebaseFilterQueryBuilder previous)
        {
            client = previous.client;
            path = previous.path;
            orderBy = previous.orderBy;
            startAt = previous.startAt;
            endAt = previous.endAt;
            equalTo = previous.equalTo;
            limitToFirst = previous.limitToFirst;
            limitToLast = previous.limitToLast;
        }

        public FirebaseFilterQueryBuilder OrderByKey()
        {
            return new FirebaseFilterQueryBuilder(this)
            {
                orderBy = "$key"
            };
        }

        public FirebaseFilterQueryBuilder OrderByValue()
        {
            return new FirebaseFilterQueryBuilder(this)
            {
                orderBy = "$value"
            };
        }

        public FirebaseFilterQueryBuilder OrderBy(String by)
        {
            return new FirebaseFilterQueryBuilder(this)
            {
                orderBy = by
            };
        }

        //TODO: are startAt and equalTo mutually Exclusive?
        public FirebaseFilterQueryBuilder StartAt(String start)
        {
            return new FirebaseFilterQueryBuilder(this)
            {
                startAt = start
            };
        }

        public FirebaseFilterQueryBuilder EndAt(String end)
        {
            return new FirebaseFilterQueryBuilder(this)
            {
                endAt = end
            };
        }

        public FirebaseFilterQueryBuilder EqualTo(String equal)
        {
            return new FirebaseFilterQueryBuilder(this)
            {
                equalTo = equal
            };
        }

        public FirebaseFilterQueryBuilder LimitToFirst(String first)
        {
            return new FirebaseFilterQueryBuilder(this)
            {
                limitToFirst = first
            };
        }

        public FirebaseFilterQueryBuilder LimitToLast(String last)
        {
            return new FirebaseFilterQueryBuilder(this)
            {
                limitToLast = last
            };
        }

        public IFirebaseQuery Build()
        {
            List<String> args = new List<string>();
            if (!orderBy.Equals("")) args.Add($"orderBy=\"{orderBy}\"");
            if (!startAt.Equals("")) args.Add($"startAt={startAt}");
            if (!endAt.Equals("")) args.Add($"endAt={endAt}");
            if (!equalTo.Equals("")) args.Add($"equalTo={equalTo}");
            if (!limitToFirst.Equals("")) args.Add($"limitToFirst={limitToFirst}");
            if (!limitToLast.Equals("")) args.Add($"limitToLast={limitToLast}");

            if (args.Count == 0)
            {
                return new FirebaseQuery(client, path);
            }
            else
            {
                return new FilterFirebaseQuery(client, path, String.Join("&", args));
            }
        }
    }
}