var leaderboardRuns = []
var leaderboardScore = []

var leaderboardRunsF = function() {
        firebase.database().ref("CloudFunctions").orderByChild("ghostRuns").limitToLast(100).on('value', function (snapshot) {
            leaderboardRuns = [];
            var unsorted = snapshot.val();
            for (var key in unsorted) {
                if (unsorted.hasOwnProperty(key)) {
                    leaderboardRuns.push({
                        "score": unsorted[key]['score'].toString(), "username": unsorted[key]['username'], "ghostRuns": unsorted[key]['ghostRuns']
});
                }
            }

            leaderboardRuns.sort(function (a, b) {
                return b["ghostRuns"] - a["ghostRuns"];
            });
            var content = ""
            for (var key in leaderboardRuns) {
                content += `<li class="list-group-item d-flex justify-content-between align-items-center">${leaderboardRuns[key]["username"]} - ${leaderboardRuns[key]["ghostRuns"]}# ghost Runs<span class="badge bg-primary">${leaderboardRuns[key]["score"]}</span>
    </li>`
            }
            mode = snapshot._delegate.ref._path.pieces_[0]
            document.getElementById("leaderboard-content-runs").innerHTML = content;
        });
}

var leaderboardScoreF = function () {
        firebase.database().ref("CloudFunctions").orderByChild("ghostRuns").limitToLast(100).on('value', function (snapshot) {
            leaderboardScore = [];
            var unsorted = snapshot.val();
            for (var key in unsorted) {
                if (unsorted.hasOwnProperty(key)) {
                    leaderboardScore.push({
                        "score": unsorted[key]['score'].toString(), "username": unsorted[key]['username'], "ghostRuns": unsorted[key]['ghostRuns']
                    });
                }
            }

            leaderboardScore.sort(function (a, b) {
                return b["score"] - a["score"];
            });
            var content = ""
            for (var key in leaderboardScore) {
                content += `<li class="list-group-item d-flex justify-content-between align-items-center">${leaderboardScore[key]["username"]} - ${leaderboardScore[key]["ghostRuns"]}# ghost Runs<span class="badge bg-primary">${leaderboardScore[key]["score"]}</span>
    </li>`
            }
            mode = snapshot._delegate.ref._path.pieces_[0]
            document.getElementById("leaderboard-content-score").innerHTML = content;
        });
}

leaderboardRunsF();
leaderboardScoreF();
